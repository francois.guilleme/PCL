#!/bin/bash

export PCLDIR=$(pwd)
 export PCLBINDIR64=$PCLDIR/build/bin
 export PCLBINDIR=$PCLBINDIR64 
 export PCLLIBDIR64=$PCLDIR/build/lib
 export PCLLIBDIR=$PCLLIBDIR64
 export PCLINCDIR=$PCLDIR/include
 export PCLSRCDIR=$PCLDIR/src

# environment vars must be defined
if [ -z ${PCLDIR+x} ] ||
   [ -z ${PCLBINDIR64+x} ] ||
   [ -z ${PCLBINDIR+x} ] ||
   [ -z ${PCLLIBDIR64+x} ] ||
   [ -z ${PCLLIBDIR+x} ] ||
   [ -z ${PCLINCDIR+x} ] ||
   [ -z ${PCLSRCDIR+x} ]; then
  echo "PCL environment variables must be defined.";
  echo "Reference: https://gitlab.com/pixinsight/PCL";
  exit
fi

# create the lib dir if not existing
[ ! -d $PCLLIBDIR64 ] && mkdir -p $PCLLIBDIR64
[ ! -d $PCLBINDIR64 ] && mkdir -p $PCLBINDIR64

PCL_MAKE_PATH="${PCLSRCDIR}/pcl/linux/g++"
LIB_3RDPARTY_SCRIPT="${PCLSRCDIR}/3rdparty/linux/make-3rdparty-libs.sh"
UTILS_SCRIPT="${PCLSRCDIR}/utils/linux/make-3rdparty-libs.sh"
FILE_FORMAT_SCRIPT="${PCLSRCDIR}/modules/file-formats/linux/make-file-formats.sh"
PROCESSES_SCRIPT="${PCLSRCDIR}/modules/processes/linux/make-processes.sh"

# build PCL
cd $PCL_MAKE_PATH
make -j16

# build 3rd party libs
source "$LIB_3RDPARTY_SCRIPT"
source "$UTILS_SCRIPT"

# build file formats
source "$FILE_FORMAT_SCRIPT"

# build processes
source "$PROCESSES_SCRIPT"
