set(SOURCES
	SandboxInstance.cpp
	SandboxInterface.cpp
	SandboxModule.cpp
	SandboxParameters.cpp
	SandboxProcess.cpp)

add_library(Sandbox-pxm SHARED ${SOURCES})
if (APPLE)
  target_link_libraries(Sandbox-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi cminpack-pxi  RFC6234-pxi "-framework AppKit" "-framework ApplicationServices")
elseif (UNIX)
  target_link_libraries(Sandbox-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi cminpack-pxi RFC6234-pxi)
endif()
