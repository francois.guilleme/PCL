set(SOURCES
	FindingChartInstance.cpp
	FindingChartInterface.cpp
	FindingChartModule.cpp
	FindingChartParameters.cpp
	FindingChartProcess.cpp)

add_library(FindingChart-pxm SHARED ${SOURCES})
if (APPLE)
  target_link_libraries(FindingChart-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi cminpack-pxi  RFC6234-pxi "-framework AppKit" "-framework ApplicationServices")
elseif (UNIX)
  target_link_libraries(FindingChart-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi cminpack-pxi RFC6234-pxi)
endif()
