set(SOURCES
	ColorManagementSetupAction.cpp
	ColorManagementSetupInstance.cpp
	ColorManagementSetupInterface.cpp
	ColorManagementSetupParameters.cpp
	ColorManagementSetupProcess.cpp
	GlobalModule.cpp
	PreferencesAction.cpp
	PreferencesInstance.cpp
	PreferencesInterface.cpp
	PreferencesParameters.cpp
	PreferencesProcess.cpp
	ReadoutOptionsAction.cpp
	ReadoutOptionsInstance.cpp
	ReadoutOptionsInterface.cpp
	ReadoutOptionsParameters.cpp
	ReadoutOptionsProcess.cpp)

add_library(Global-pxm SHARED ${SOURCES})
if (APPLE)
  target_link_libraries(Global-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi cminpack-pxi  RFC6234-pxi "-framework AppKit" "-framework ApplicationServices")
elseif (UNIX)
  target_link_libraries(Global-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi cminpack-pxi RFC6234-pxi)
endif()
