set(SOURCES
	ChannelCombinationInstance.cpp
	ChannelCombinationInterface.cpp
	ChannelCombinationParameters.cpp
	ChannelCombinationProcess.cpp
	ChannelExtractionInstance.cpp
	ChannelExtractionInterface.cpp
	ChannelExtractionParameters.cpp
	ChannelExtractionProcess.cpp
	ChannelParameters.cpp
	ChannelSourceSelectionDialog.cpp
	ColorSpacesModule.cpp
	ExtractCIELAction.cpp
	GrayscaleRGBActions.cpp
	GrayscaleRGBInstances.cpp
	GrayscaleRGBProcesses.cpp
	LRGBCombinationInstance.cpp
	LRGBCombinationInterface.cpp
	LRGBCombinationParameters.cpp
	LRGBCombinationProcess.cpp
	RGBWorkingSpaceInstance.cpp
	RGBWorkingSpaceInterface.cpp
	RGBWorkingSpaceParameters.cpp
	RGBWorkingSpaceProcess.cpp
	SplitRGBChannelsAction.cpp)

add_library(ColorSpaces-pxm SHARED ${SOURCES})
if (APPLE)
  target_link_libraries(ColorSpaces-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi  RFC6234-pxi "-framework AppKit" "-framework ApplicationServices")
elseif (UNIX)
  target_link_libraries(ColorSpaces-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi RFC6234-pxi)
endif()
