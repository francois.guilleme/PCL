set(SOURCES
	AssignICCProfileInstance.cpp
	AssignICCProfileInterface.cpp
	AssignICCProfileParameters.cpp
	AssignICCProfileProcess.cpp
	ColorManagementModule.cpp
	ICCProfileTransformationInstance.cpp
	ICCProfileTransformationInterface.cpp
	ICCProfileTransformationParameters.cpp
	ICCProfileTransformationProcess.cpp)

add_library(ColorManagement-pxm SHARED ${SOURCES})
if (APPLE)
  target_link_libraries(ColorManagement-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi  RFC6234-pxi "-framework AppKit" "-framework ApplicationServices")
elseif (UNIX)
  target_link_libraries(ColorManagement-pxm PCL-pxi lz4-pxi zlib-pxi lcms-pxi RFC6234-pxi)
endif()
