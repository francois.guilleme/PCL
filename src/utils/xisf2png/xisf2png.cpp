#include "pcl/Image.h"
#include <cstdio>
#include <iostream>

#include <png.h>

#include <pcl/XISF.h>

using namespace pcl;

class SilentLogHandler : public XISFLogHandler {
public:
  SilentLogHandler() = default;

  virtual void Init(const String &filePath, bool writing) {}

  virtual void Log(const String &text, message_type type) {
    switch (type) {
    default:
    case XISFMessageType::Informative:
    case XISFMessageType::Note:
      break;
    case XISFMessageType::Warning:
      std::cerr << "** " << text;
      break;
    case XISFMessageType::RecoverableError:
      std::cerr << "*** " << text;
      break;
    }
  }

  virtual void Close() {}
};

static bool OutputPNG(const char *path, const char *out, int scale) {
  XISFReader xisf;
  xisf.SetLogHandler(new SilentLogHandler);
  xisf.Open(String(path));

  if (xisf.NumberOfImages() < 1) {
    std::cerr << "The XSIF unit contains no readable image.\n";
    return false;
  }

  FILE *fp = fopen(out, "wb");
  if (!fp) {
    std::cerr << "error open output\n";
    return false;
  }

  png_structp png =
      png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png) {
    std::cerr << "OOPS: failed create png struct\n";
    return false;
  }

  png_infop png_info = png_create_info_struct(png);
  if (!png_info) {
    std::cerr << "OOPS: failed create png info struct\n";
    return false;
  }

  int w = xisf.ImageInfo().width;
  int h = xisf.ImageInfo().height;

  png_init_io(png, fp);
  png_set_IHDR(png, png_info, w / scale, h / scale, 8, PNG_COLOR_TYPE_RGBA,
               PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
               PNG_FILTER_TYPE_DEFAULT);
  png_write_info(png, png_info);
  png_byte *data = new png_byte[w * h * 4 / (scale * scale)];
  png_bytep ptr = &data[0];
  png_bytep rows[h / scale];
  for (int y = 0; y < h / scale; y++) {
    rows[y] = ptr;
    ptr += w * 4 / scale;
  }

  UInt8Image im;
  xisf.ReadImage(im);

  int offset = 0;
  for (int row = 0; row < h; row += scale) {
    for (int col = 0; col < w; col += scale) {
      data[offset++] = im.Pixel(col, row, 0);
      data[offset++] = im.Pixel(col, row, 1);
      data[offset++] = im.Pixel(col, row, 2);
      data[offset++] = 255;
    }
  }

  png_write_image(png, rows);
  png_write_end(png, NULL);

  fclose(fp);

  png_destroy_write_struct(&png, &png_info);

  xisf.Close();
  return true;
}

int main(int argc, const char **argv) {
  Exception::DisableGUIOutput();
  Exception::EnableConsoleOutput();
  const char *appname = argv[0];

  int scale = 1;
  if (strcmp(argv[1], "-s") == 0) {
    scale = atoi(argv[2]);
    if (scale < 1)
      scale = 1;
    argv += 2;
    argc -= 2;
  }

  if (argc != 3) {
    std::cout << "Usage: " << appname << R"( [-s <scaling>] <file> <out>
<scaling>         scaling factor
<file>            xisf to open 
<out>             output png

written by francois@guilleme.net
)" __DATE__ "\n";
    exit(1);
  }
  bool rc = OutputPNG(argv[1], argv[2], scale);
  return rc ? 0 : 1;
}
