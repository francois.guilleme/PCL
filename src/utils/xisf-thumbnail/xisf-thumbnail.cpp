#include <cstdio>
#include <iostream>

#include <png.h>

#include <pcl/XISF.h>

using namespace pcl;

class SilentLogHandler : public XISFLogHandler {
public:
  SilentLogHandler() = default;

  virtual void Init(const String &filePath, bool writing) {}

  virtual void Log(const String &text, message_type type) {
    switch (type) {
    default:
    case XISFMessageType::Informative:
    case XISFMessageType::Note:
      break;
    case XISFMessageType::Warning:
      std::cerr << "** " << text;
      break;
    case XISFMessageType::RecoverableError:
      std::cerr << "*** " << text;
      break;
    }
  }

  virtual void Close() {}
};

static bool OutputThumbnail(const char *path, const char *out) {
  XISFReader xisf;
  xisf.SetLogHandler(new SilentLogHandler);
  xisf.Open(String(path));

  if (xisf.NumberOfImages() < 1) {
    std::cerr << "The XSIF unit contains no readable image.\n";
    return false;
  }

  const auto image = xisf.ReadThumbnail();

  FILE *fp = fopen(out, "wb");
  if (!fp) {
    std::cerr << "error open output\n";
    return false;
  }

  png_structp png =
      png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png) {
    std::cerr << "OOPS: failed create png struct\n";
    return false;
  }

  png_infop png_info = png_create_info_struct(png);
  if (!png_info) {
    std::cerr << "OOPS: failed create png info struct\n";
    return false;
  }

  int w = image.Width();
  int h = image.Height();

  png_init_io(png, fp);
  png_set_IHDR(png, png_info, w, h, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
  png_write_info(png, png_info);
  png_byte *data = new png_byte[w * h * 4];
  png_bytep ptr = &data[0];
  png_bytep rows[h];
  for (int y = 0; y < h; y++) {
    rows[y] = ptr;
    ptr += w * 4;
  }

  int offset = 0;
  for (int row = 0; row < h; ++row) {
    for (int col = 0; col < w; ++col) {
      data[offset++] = image.Pixel(col, row, 0);
      data[offset++] = image.Pixel(col, row, 1);
      data[offset++] = image.Pixel(col, row, 2);
      data[offset++] = 255;
    }
  }

  png_write_image(png, rows);
  png_write_end(png, NULL);

  fclose(fp);

  png_destroy_write_struct(&png, &png_info);

  xisf.Close();
  return true;
}

int main(int argc, const char **argv) {
  Exception::DisableGUIOutput();
  Exception::EnableConsoleOutput();

  if (argc != 3) {
    std::cout << "Usage: " << argv[0] << R"( <file> <out>
<file>            xisf to open 
<out>             output thumbnail png

written by francois@guilleme.net
)" __DATE__ "\n";
    exit(1);
  }

  bool rc = OutputThumbnail(argv[1], argv[2]);
  return rc ? 0 : 1;
}
